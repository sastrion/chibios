/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

                                      ---

    A special exception to the GPL can be applied should you wish to distribute
    a combined work that includes ChibiOS/RT, without being obliged to provide
    the source code for any proprietary components. See the file exception.txt
    for full details of how and when the exception can be applied.
*/

/**
 * @file    Posix/serial_lld.c
 * @brief   Posix low level simulated serial driver code.
 *
 * @addtogroup POSIX_SERIAL
 * @{
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "ch.h"
#include "hal.h"

#if HAL_USE_SERIAL || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/** @brief Serial driver 1 identifier.*/
#if USE_SIM_SERIAL1 || defined(__DOXYGEN__)
SerialDriver SD1;
#endif
/** @brief Serial driver 2 identifier.*/
#if USE_SIM_SERIAL2 || defined(__DOXYGEN__)
SerialDriver SD2;
#endif
/** @brief Serial driver 3 identifier.*/
#if USE_SIM_SERIAL2 || defined(__DOXYGEN__)
SerialDriver SD3;
#endif
/** @brief Serial driver 4 identifier.*/
#if USE_SIM_SERIAL4 || defined(__DOXYGEN__)
SerialDriver SD4;
#endif
/** @brief Serial driver 5 identifier.*/
#if USE_SIM_SERIAL5 || defined(__DOXYGEN__)
SerialDriver SD5;
#endif
/** @brief Serial driver 6 identifier.*/
#if USE_SIM_SERIAL6 || defined(__DOXYGEN__)
SerialDriver SD6;
#endif


/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

//static fd_set rfds;
//static fd_set wfds;

/** @brief Driver default configuration.*/
static const SerialConfig default_config = {
};

static u_long nb = 1;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

static void init(SerialDriver *sdp, uint16_t port) {
  struct sockaddr_in sad;
  struct protoent *prtp;

  if ((prtp = getprotobyname("tcp")) == NULL) {
    printf("%s: Error mapping protocol name to protocol number\n", sdp->com_name);
    goto abort;
  }

  sdp->com_listen = socket(PF_INET, SOCK_STREAM, prtp->p_proto);
  if (sdp->com_listen == INVALID_SOCKET) {
    printf("%s: Error creating simulator socket\n", sdp->com_name);
    goto abort;
  }

  if (ioctl(sdp->com_listen, FIONBIO, &nb) != 0) {
    printf("%s: Unable to setup non blocking mode on socket\n", sdp->com_name);
    goto abort;
  }

  fcntl(sdp->com_listen, F_SETOWN, getpid());
  int oflags = fcntl(sdp->com_listen, F_GETFL);
  fcntl(sdp->com_listen, F_SETFL, oflags | FASYNC);

  memset(&sad, 0, sizeof(sad));
  sad.sin_family = AF_INET;
  sad.sin_addr.s_addr = INADDR_ANY;
  sad.sin_port = htons(port);

  int optval = 1;
  setsockopt(sdp->com_listen, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

  if (bind(sdp->com_listen, (struct sockaddr *)&sad, sizeof(sad))) {
    printf("%s: Error binding socket\n", sdp->com_name);
    goto abort;
  }

  if (listen(sdp->com_listen, 1) != 0) {
    printf("%s: Error listening socket\n", sdp->com_name);
    goto abort;
  }

  printf("Channel %s listening on port %d\n", sdp->com_name, port);
  return;

abort:
  if (sdp->com_listen != INVALID_SOCKET)
    close(sdp->com_listen);
  exit(1);
}

static void connint(SerialDriver *sdp) {

  if (sdp->com_data != INVALID_SOCKET)
    return;

  struct sockaddr addr;
  socklen_t addrlen = sizeof(addr);

  if ((sdp->com_data = accept(sdp->com_listen, &addr, &addrlen)) == INVALID_SOCKET)
    return;

  if (ioctl(sdp->com_data, FIONBIO, &nb) != 0) {
    printf("%s: Unable to setup non blocking mode on data socket\n", sdp->com_name);
    goto abort;
  }

  // allow for SIGIO on com_data socket
  fcntl(sdp->com_data, F_SETOWN, getpid());
  int oflags = fcntl(sdp->com_data, F_GETFL);
  fcntl(sdp->com_data, F_SETFL, oflags | FASYNC);

  chSysLockFromIsr();
  chIOAddFlagsI(sdp, IO_CONNECTED);
  chSysUnlockFromIsr();
  return;

abort:
  if (sdp->com_listen != INVALID_SOCKET)
    close(sdp->com_listen);
  if (sdp->com_data != INVALID_SOCKET)
    close(sdp->com_data);
  exit(1);
}

static void inint(SerialDriver *sdp) {

  if (sdp->com_data == INVALID_SOCKET)
    return;

  int i;
  uint8_t data[SERIAL_BUFFERS_SIZE];

  int n = recv(sdp->com_data, data, sizeof(data), 0);
  switch (n) {
  case 0:
    close(sdp->com_data);
    sdp->com_data = INVALID_SOCKET;
    chSysLockFromIsr();
    chIOAddFlagsI(sdp, IO_DISCONNECTED);
    chSysUnlockFromIsr();
    return;
  case INVALID_SOCKET:
	if (errno == EWOULDBLOCK)
	  return;
    close(sdp->com_data);
    sdp->com_data = INVALID_SOCKET;
    return;
  }

  chSysLockFromIsr();
  for (i = 0; i < n; i++) {
    if (chIQIsEmptyI(&sdp->iqueue))
      chIOAddFlagsI(sdp, IO_INPUT_AVAILABLE);
    if (chIQPutI(&sdp->iqueue, data[i]) < Q_OK) {
      chIOAddFlagsI(sdp, SD_OVERRUN_ERROR);
      break;
    }
  }
  chSysUnlockFromIsr();
}

static void outint(SerialDriver *sdp) {
  int32_t n, i = -1;
  uint8_t data[SERIAL_BUFFERS_SIZE];

  if (sdp->com_data == INVALID_SOCKET)
    return;

  do {
    chSysLockFromIsr();
    n = chOQGetI(&sdp->oqueue);
    chSysUnlockFromIsr();
    if (n < Q_OK) {
      chIOAddFlagsI(sdp, IO_OUTPUT_EMPTY);
      break;
    }
    i += 1;
    data[i] = n;
  } while (i < SERIAL_BUFFERS_SIZE - 1);

  if (i < 0)
    return;

  n = send(sdp->com_data, data, i+1, 0);
  switch (n) {
  case 0:
    close(sdp->com_data);
    sdp->com_data = INVALID_SOCKET;
    chSysLockFromIsr();
    chIOAddFlagsI(sdp, IO_DISCONNECTED);
    chSysUnlockFromIsr();
    return;
  case INVALID_SOCKET:
    close(sdp->com_data);
    sdp->com_data = INVALID_SOCKET;
    return;
  }
}

static void deinit(SerialDriver *sdp) {
  if (sdp->com_data != INVALID_SOCKET) {
    close(sdp->com_data);
    sdp->com_data = INVALID_SOCKET;
  }
  close(sdp->com_listen);
  sdp->com_listen = INVALID_SOCKET;
  printf("Channel %s closed\n", sdp->com_name);
}

/*===========================================================================*/
/* Driver interrupt handlers.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

void sd_lld_handle_irq(int sig, siginfo_t *si, void *uc) {

  (void)sig;
  (void)si;
  (void)uc;

  CH_IRQ_PROLOGUE();

#ifdef USE_SIM_SERIAL1
  connint(&SD1);
  inint(&SD1);
  outint(&SD1);
#endif

#ifdef USE_SIM_SERIAL2
  connint(&SD2);
  inint(&SD2);
  outint(&SD2);
#endif

#ifdef USE_SIM_SERIAL3
  connint(&SD3);
  inint(&SD3);
  outint(&SD3);
#endif

  CH_IRQ_EPILOGUE();

  dbg_check_lock();
  if (chSchIsPreemptionRequired())
    chSchDoReschedule();
  dbg_check_unlock();

}

static void notify(GenericQueue *qp) {
  (void) qp;
  raise(SIGIO);
}

/**
 * @brief   Low level serial driver initialization.
 */
void sd_lld_init(void) {
  struct sigaction sa;
  sa.sa_flags = SA_SIGINFO | SA_RESTART;
  sa.sa_sigaction = sd_lld_handle_irq;
  sigemptyset(&sa.sa_mask);
  sigaddset(&sa.sa_mask, SIGALRM);
  if (sigaction(SIGIO, &sa, NULL) == -1)
	  puts("error sigaction");

#if USE_SIM_SERIAL1
  sdObjectInit(&SD1, NULL, notify);
  SD1.com_listen = INVALID_SOCKET;
  SD1.com_data = INVALID_SOCKET;
  SD1.com_name = "SD1";
#endif

#if USE_SIM_SERIAL2
  sdObjectInit(&SD2, NULL, notify);
  SD2.com_listen = INVALID_SOCKET;
  SD2.com_data = INVALID_SOCKET;
  SD2.com_name = "SD2";
#endif

#if USE_SIM_SERIAL3
  sdObjectInit(&SD3, NULL, notify);
  SD3.com_listen = INVALID_SOCKET;
  SD3.com_data = INVALID_SOCKET;
  SD3.com_name = "SD3";
#endif

}

/**
 * @brief   Low level serial driver configuration and (re)start.
 *
 * @param[in] sdp       pointer to a @p SerialDriver object
 */
void sd_lld_start(SerialDriver *sdp, const SerialConfig *config) {

  if (config == NULL)
    config = &default_config;

#if USE_SIM_SERIAL1
  if (sdp == &SD1) {
    init(&SD1, SIM_SD1_PORT);
  }
#endif

#if USE_SIM_SERIAL2
  if (sdp == &SD2) {
    init(&SD2, SIM_SD2_PORT);
  }
#endif

#if USE_SIM_SERIAL3
  if (sdp == &SD3) {
    init(&SD3, SIM_SD3_PORT);
  }
#endif
}

/**
 * @brief   Low level serial driver stop.
 * @details De-initializes the USART, stops the associated clock, resets the
 *          interrupt vector.
 *
 * @param[in] sdp       pointer to a @p SerialDriver object
 */
void sd_lld_stop(SerialDriver *sdp) {

#if USE_SIM_SERIAL1
  if (sdp == &SD1) {
    deinit(&SD1);
  }
#endif

#if USE_SIM_SERIAL2
  if (sdp == &SD2) {
    deinit(&SD2);
  }
#endif

#if USE_SIM_SERIAL3
  if (sdp == &SD3) {
    deinit(&SD3);
  }
#endif
}

#endif /* HAL_USE_SERIAL */

/** @} */
